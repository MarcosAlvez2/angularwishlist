import {DestinoViaje} from './destino-viaje.model';
import {Action} from '@ngrx/store';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {HttpClientModule} from '@angular/common/http';
import {d} from '@angular/core/src/render3';


export interface DestinosViajesState {
  items: DestinoViaje[];
  loading: boolean;
  favorito: DestinoViaje;
}

export function initializeDestinosViajesState() {
  return {
    items: [],
    loading: false,
    favorito: null
  };
}

// Actions

export enum DestinoViajesActionTypes {
  NUEVO_DESTINO = '[Destinos Viajes] Nuevo',
  ELEGIDO_FAVORITO = '[Destinos Viajes] Favorito',
  VOTE_UP = '[Destinos Viajes] Vote Up',
  VOTE_DOWN = '[Destinos Viajes] Vote Down',
  VOTE_RESET = '[Destinos Viajes] Vote Reset',
  INIT_MY_DATA = '[Destinos Viaje] Init My Data'
}

export class NuevoDestinoAction implements Action {
  type = DestinoViajesActionTypes.NUEVO_DESTINO;
  constructor(public destino: DestinoViaje) {}
}

export class ElegidoFavoritoAction implements Action {
  type = DestinoViajesActionTypes.ELEGIDO_FAVORITO;
  constructor(public destino: DestinoViaje) {}
}

export class VoteUpAction implements Action {
  type = DestinoViajesActionTypes.VOTE_UP;
  constructor(public destino: DestinoViaje) {}
}

export class VoteDownAction implements Action {
  type = DestinoViajesActionTypes.VOTE_DOWN;
  constructor(public destino: DestinoViaje) {}
}

export class VoteResetAction implements Action {
  type = DestinoViajesActionTypes.VOTE_RESET;
  constructor(public destino: DestinoViaje) {}
}

export class InitMyDataAction implements Action {
  type = DestinoViajesActionTypes.INIT_MY_DATA;
  constructor(public destinos: string[]) {}
}

export type DestinosViajesAction = NuevoDestinoAction | ElegidoFavoritoAction | VoteUpAction
  | VoteDownAction | VoteResetAction | InitMyDataAction;

// Reducers

export function reducerDestinosViajes(
  state: DestinosViajesState,
  action: DestinosViajesAction
): DestinosViajesState {
  switch (action.type) {
    case DestinoViajesActionTypes.INIT_MY_DATA: {
      const destinos: string[] = (action as InitMyDataAction).destinos;
      return {
        ...state,
        items: destinos.map((da) => new DestinoViaje(da, ''))
      };
    }
    case DestinoViajesActionTypes.NUEVO_DESTINO: {
      return {
        ...state,
        items: [...state.items, (action as NuevoDestinoAction).destino]
      };
    }
    case DestinoViajesActionTypes.ELEGIDO_FAVORITO: {
      state.items.forEach(x => x.setSelected(false));
      const fav: DestinoViaje = (action as ElegidoFavoritoAction).destino;
      fav.setSelected(true);
      return {
        ...state,
        favorito: fav
      };
    }
    case DestinoViajesActionTypes.VOTE_UP: {
      (action as VoteUpAction).destino.voteUp();
      return {...state};
    }
    case DestinoViajesActionTypes.VOTE_DOWN: {
      (action as VoteDownAction).destino.voteDown();
      return {...state};
    }
    case DestinoViajesActionTypes.VOTE_RESET: {
      (action as VoteDownAction).destino.voteReset();
      return {...state};
    }
  }
  return state;
}

// Effects
@Injectable()
export class DestinoViajesEffects {
  @Effect()
  nuevoAgregado$: Observable<Action> = this.actions$.pipe(
    ofType(DestinoViajesActionTypes.NUEVO_DESTINO),
    map((action: NuevoDestinoAction) => new ElegidoFavoritoAction(action.destino))
  );

  constructor(private actions$: Actions) {}
}
