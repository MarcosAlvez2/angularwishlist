import { EventEmitter, Output } from '@angular/core';
import { Component, OnInit, Input, HostBinding } from '@angular/core';
import {Store} from '@ngrx/store';
import {VoteDownAction, VoteResetAction, VoteUpAction} from '../../models/destinos-viaje-state.model';
import {DestinoViaje} from '../../models/destino-viaje.model';
import {AppState} from '../../app.module';
import {animate, state, style, transition, trigger} from '@angular/animations';

@Component({
  selector: 'app-destino-viaje',
  templateUrl: './destino-viaje.component.html',
  styleUrls: ['./destino-viaje.component.css'],
  animations: [
    trigger('esFavorito', [
      state('estadoFavorito', style({backgroundColor: 'PaleTurquoise'})),
      state('estadoNoFavorito', style({backgroundColor: 'Whitesmoke'})),
      transition('estadoNoFavorito => estadoFavorito', [
        animate('3s')
      ]),
      transition('estadoFavorito => estadoNoFavorito', [
        animate('1s')
      ])
    ])
  ]
})
export class DestinoViajeComponent implements OnInit {

  @Input() destino: DestinoViaje;
  @HostBinding('attr.class') cssClass = 'col-md-4';
  @Output() clicked: EventEmitter<DestinoViaje>;
  // tslint:disable-next-line:no-input-rename
  @Input('idx') position: number;

  constructor(private store: Store<AppState>) {
    this.clicked = new EventEmitter();
  }

  ngOnInit() {
  }

  ir() {
    this.clicked.emit(this.destino);
    return false;
  }

  voteUp() {
    this.store.dispatch(new VoteUpAction(this.destino));
    return false;
  }

  voteDown() {
    this.store.dispatch(new VoteDownAction(this.destino));
    return false;
  }

  voteReset() {
    this.store.dispatch(new VoteResetAction(this.destino));
    return false;
  }
}
